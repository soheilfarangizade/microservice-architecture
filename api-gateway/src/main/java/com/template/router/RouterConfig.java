package com.template.router;

import org.springframework.cloud.gateway.route.RouteLocator;
import org.springframework.cloud.gateway.route.builder.RouteLocatorBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.codec.ServerCodecConfigurer;

@Configuration
public class RouterConfig {


    @Bean
    public RouteLocator routeLocator(RouteLocatorBuilder builder) {

        /*return builder.routes()
                .route("account-service", r -> r.path("/account/**").uri("lb://account-service"))
                .route("auth-service", r -> r.path("/auth/**").uri("lb://auth-service"))
                .build();*/
        return null;
    }



    @Bean
    public ServerCodecConfigurer serverCodecConfigurer() {
        return ServerCodecConfigurer.create();
    }
}
